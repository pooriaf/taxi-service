# Taxi Service

## How to setup on the local environment

Prerequisites:  
- PHP
- MySQL
- Composer
- NodeJS (for client-side dependencies)
- Yarn/npm (for client-side dependencies)
- Symfony binary (for temporary webserver)

After cloning the project, open the project root directory.

Execute following command to install project dependencies:
```sh
$ composer install 
```
Open .env file and set the database credentials and details (You can also have your own .env.local)
```sh
DATABASE_URL=mysql://username:password@127.0.0.1:3306/taxi?serverVersion=8
```
Create database by the command:
```sh
$ php bin/console doctrine:database:create
```
Create database tables through existing migrations:
```sh
$ php bin/console doctrine:migrations:migrate
```
Seed database tables with initial values (airports, terminals):
```sh
$ php bin/console doctrine:fixtures:load
```
Execute following command to install client-side dependencies:
```sh
$ yarn install 
```
After installing client dependencies using yarm, Execute following command to compile assets:
```sh
$ yarn encore dev
```
You can use symfony local webserver:
```sh
$ symfony server:start
```
## How to run tests
set database setting in .env.test and execute following commands to create specific database for test env which will be used in functional tests.
```sh
$ php bin/console doctrine:database:create --env=test
$ php bin/console doctrine:migrations:migrate --env=test
```
in case of using PHP 7.5 or higher, change `phpunit.xml` based on its comments.

It will be possible to run all the tests through the following command (It also installs dependencies in the first call):
```sh
$ php ./bin/phpunit
```
This project includes different unit and functional tests. In order to perform the application tests that the database is involved in, configurations have been made that can refresh the database at the time of each test execution.

## About the project

#### API Endpoints
In this project, the basic information of airports and terminals is defined dynamically, 
since in real projects, this information should be editable and can be reported on them, 
instead of using static information for this part, this information is stored in a normalized structure in the database.
(Keep in mind that the terminals themselves will not necessarily be static numbers)
Following restful endpoints are available to provide those:
```sh
GET /api/airports
GET /api/airports/{id}/terminals
```
and an endpoint to submit the form:
```sh
POST /api/taxi-requests
```

#### How sending emails works
It is essential that operations such as sending text messages, emails, etc., which may be time consuming or unsuccessful, be performed async. so the symfony/messenger package has been added to the project.
To activate it, just uncomment the following line in the `messenger.yaml` and config the preferred transport mechanism through env file.
```sh
'Symfony\Component\Mailer\Messenger\SendEmailMessage': 'async'
```
by default, following config is set on the env file which leads to bypass the sending for development/testing purposes.
```sh
MAILER_DSN=null://null
```

#### Form validations
In this project, all validations have been implemented on both server-side and client-side according to the requirements.
Of course, it is preferable for fields whose validation has many changes, such as air flight code, to do so only on the server side, which is easier to manage.

#### Client-side
Bootstrap and VueJS have been used in the client-side.
The back-end and front-end of the software are both developed in the form of this project, 
although in larger projects these two parts can be completely separated.
The `Axios` package is used for API communication instead of the native `fetch` as it is not supported by all browsers.
## Future works
  - Adding support for different forms of UK mobiles (including country code, spaces,...) and unify them on the server side
  - More granularity in VueJS components
  - Handling network errors in API calls
  - Adding SCSS preprocessor and compile bootstrap css through that
  - Following a CSS naming convention like BEM when the project grows
  - Adding client-side tests

## Live demo
To deploy the project on a live server, a cloud service has been configured.
Access to the live demo from the following IP address:
[http://194.5.193.83/](http://194.5.193.83/)



**Looking forward hearing back from you!**

