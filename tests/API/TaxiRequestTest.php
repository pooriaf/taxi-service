<?php

namespace App\Tests\API;

use App\DataFixtures\AirportFixtures;
use App\Entity\Airport;
use App\Lib\Test\TaxiServiceTrait;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaxiRequestTest extends WebTestCase
{
    use TaxiServiceTrait;

    private $kernelBrowser;

    protected function setUp(): void
    {
        $this->kernelBrowser = self::createClient();
    }

    public function testAirportsIndex()
    {
        $this->kernelBrowser->request('GET', '/api/airports');
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testAirportsTerminalsIndex()
    {
        $this->kernelBrowser->request('GET', '/api/airports/1/terminals');
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());
    }

    public function testStoreTaxiRequest()
    {
        /** @var EntityManager $entityManager */
        $entityManager = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $fixture = new AirportFixtures();
        $fixture->load($entityManager);
        $taxiRequest = $this->getDummyRequestArray($entityManager->getRepository(Airport::class)->findOneBy([])->getId());
        $this->kernelBrowser->request('POST', '/api/taxi-requests', [], [], [], json_encode($taxiRequest));
        $this->assertEquals(200, $this->kernelBrowser->getResponse()->getStatusCode());
    }
}