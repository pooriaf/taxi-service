<?php

namespace App\Tests\Validator;

use App\Lib\Test\TaxiServiceTrait;
use App\Validator\TaxiRequestValidator;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class TaxiRequestValidatorTest extends KernelTestCase
{
    use TaxiServiceTrait;

    private $validator;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->validator = $kernel->getContainer()->get('validator');
    }

    public function testValidInputAsserted()
    {
        $violations = $this->validator->validate($this->getDummyRequestArray(), TaxiRequestValidator::getCollection());
        $this->assertCount(0, $violations);
    }

    public function testNonUKMobileNotAllowed()
    {
        $input = $this->getDummyRequestArray();
        $input['mobile'] = '09122626637';
        /** @var ConstraintViolation[] $violations */
        $violations = $this->validator->validate($input, TaxiRequestValidator::getCollection());
        $this->assertCount(1, $violations);
        $this->assertEquals('[mobile]', $violations[0]->getPropertyPath());
    }

    public function testInvalidAirFlightNumberNotAllowed()
    {
        $input = $this->getDummyRequestArray();
        $input['airflight_number'] = '2AAAA333';
        /** @var ConstraintViolation[] $violations */
        $violations = $this->validator->validate($input, TaxiRequestValidator::getCollection());
        $this->assertCount(1, $violations);
        $this->assertEquals('[airflight_number]', $violations[0]->getPropertyPath());
    }

    public function testTerminalShouldBeNullOrInteger()
    {
        $input = $this->getDummyRequestArray();
        $input['terminal_id'] = null;
        /** @var ConstraintViolation[] $violations */
        $violations = $this->validator->validate($input, TaxiRequestValidator::getCollection());
        $this->assertCount(0, $violations);
        $input['terminal_id'] = 1;
        $violations = $this->validator->validate($input, TaxiRequestValidator::getCollection());
        $this->assertCount(0, $violations);
    }
}