<?php

namespace App\Tests\TaxiService;

use App\DataFixtures\AirportFixtures;
use App\Entity\Airport;
use App\Entity\TaxiRequest;
use App\Lib\Test\TaxiServiceTrait;
use App\TaxiService\TaxiRequestManager;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class TaxiRequestManagerTest extends KernelTestCase
{
    use TaxiServiceTrait;

    /** @var EntityManager */
    private $entityManager;
    /** @var TaxiRequestManager */
    private $taxiRequestManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->taxiRequestManager = self::$container->get(TaxiRequestManager::class);
    }

    public function testRequestCreatedCorrectly()
    {
        $fixture = new AirportFixtures();
        $fixture->load($this->entityManager);
        $input = $this->getDummyRequestArray($this->entityManager->getRepository(Airport::class)->findOneBy([])->getId());
        $this->taxiRequestManager->make($input);
        $taxiRequestRepository = $this->entityManager->getRepository(TaxiRequest::class);
        $taxiRequests = $taxiRequestRepository->findAll();
        $this->assertCount(1, $taxiRequests);
        $this->assertEquals($input['full_name'], $taxiRequests[0]->getFullName());
        $this->assertEquals($input['mobile'], $taxiRequests[0]->getMobile());
        $this->assertEquals($input['airport_id'], $taxiRequests[0]->getAirport()->getId());
        $this->assertEquals($input['terminal_id'], $taxiRequests[0]->getTerminal());
        $this->assertEquals($input['arrival_date'], $taxiRequests[0]->getArrivalDate()->format('Y-m-d'));
        $this->assertEquals($input['airflight_number'], $taxiRequests[0]->getAirflightnumber());
    }

    public function testNotExistingAirportNotAllowed()
    {
        $this->expectException(UnprocessableEntityHttpException::class);
        $this->taxiRequestManager->make($this->getDummyRequestArray(1));
    }

    public function testArrivalDateAfterTodayNotAllowed()
    {
        $fixture = new AirportFixtures();
        $fixture->load($this->entityManager);
        $dummyRequestArray = $this->getDummyRequestArray($this->entityManager->getRepository(Airport::class)->findOneBy([])->getId());
        $dummyRequestArray['arrival_date'] = '2020-01-01';
        $this->expectException(UnprocessableEntityHttpException::class);
        $this->taxiRequestManager->make($dummyRequestArray);
    }
}