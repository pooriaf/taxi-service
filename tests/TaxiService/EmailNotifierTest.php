<?php

namespace App\Tests\TaxiService;

use App\DataFixtures\AirportFixtures;
use App\Entity\Airport;
use App\Lib\Test\TaxiServiceTrait;
use App\TaxiService\EmailNotifier;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class EmailNotifierTest extends KernelTestCase
{
    use TaxiServiceTrait;

    /** @var EntityManager  */
    private $entityManager;
    /** @var EmailNotifier */
    private $emailNotifier;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->emailNotifier = self::$container->get(EmailNotifier::class);
    }

    public function testNotificationMailIsSentCorrectly()
    {
        $fixture = new AirportFixtures();
        $fixture->load($this->entityManager);
        /** @var Airport $airport */
        $airport = $this->entityManager->getRepository(Airport::class)->findOneBy([]);
        $this->emailNotifier->send($this->getDummyRequestObject($airport));
        $this->assertEmailCount(1);
        $email = $this->getMailerMessage(0);
        $this->assertEmailTextBodyContains($email, 'New Taxi Request Received!');
    }
}