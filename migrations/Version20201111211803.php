<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201111211803 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE airport_terminals DROP FOREIGN KEY FK_86660BD38DB82A2');
        $this->addSql('DROP INDEX IDX_86660BD38DB82A2 ON airport_terminals');
        $this->addSql('ALTER TABLE airport_terminals CHANGE airport_id_id airport_id INT NOT NULL');
        $this->addSql('ALTER TABLE airport_terminals ADD CONSTRAINT FK_86660BD3289F53C8 FOREIGN KEY (airport_id) REFERENCES airports (id)');
        $this->addSql('CREATE INDEX IDX_86660BD3289F53C8 ON airport_terminals (airport_id)');
        $this->addSql('ALTER TABLE taxi_requests DROP FOREIGN KEY FK_1C916B648DB82A2');
        $this->addSql('ALTER TABLE taxi_requests DROP FOREIGN KEY FK_1C916B64CF2FC8D0');
        $this->addSql('DROP INDEX IDX_1C916B648DB82A2 ON taxi_requests');
        $this->addSql('DROP INDEX IDX_1C916B64CF2FC8D0 ON taxi_requests');
        $this->addSql('ALTER TABLE taxi_requests CHANGE airport_id_id airport_id INT NOT NULL, CHANGE terminal_id_id terminal_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE taxi_requests ADD CONSTRAINT FK_1C916B64289F53C8 FOREIGN KEY (airport_id) REFERENCES airports (id)');
        $this->addSql('ALTER TABLE taxi_requests ADD CONSTRAINT FK_1C916B64E77B6CE8 FOREIGN KEY (terminal_id) REFERENCES airport_terminals (id)');
        $this->addSql('CREATE INDEX IDX_1C916B64289F53C8 ON taxi_requests (airport_id)');
        $this->addSql('CREATE INDEX IDX_1C916B64E77B6CE8 ON taxi_requests (terminal_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE airport_terminals DROP FOREIGN KEY FK_86660BD3289F53C8');
        $this->addSql('DROP INDEX IDX_86660BD3289F53C8 ON airport_terminals');
        $this->addSql('ALTER TABLE airport_terminals CHANGE airport_id airport_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE airport_terminals ADD CONSTRAINT FK_86660BD38DB82A2 FOREIGN KEY (airport_id_id) REFERENCES airports (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_86660BD38DB82A2 ON airport_terminals (airport_id_id)');
        $this->addSql('ALTER TABLE taxi_requests DROP FOREIGN KEY FK_1C916B64289F53C8');
        $this->addSql('ALTER TABLE taxi_requests DROP FOREIGN KEY FK_1C916B64E77B6CE8');
        $this->addSql('DROP INDEX IDX_1C916B64289F53C8 ON taxi_requests');
        $this->addSql('DROP INDEX IDX_1C916B64E77B6CE8 ON taxi_requests');
        $this->addSql('ALTER TABLE taxi_requests CHANGE airport_id airport_id_id INT NOT NULL, CHANGE terminal_id terminal_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE taxi_requests ADD CONSTRAINT FK_1C916B648DB82A2 FOREIGN KEY (airport_id_id) REFERENCES airports (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE taxi_requests ADD CONSTRAINT FK_1C916B64CF2FC8D0 FOREIGN KEY (terminal_id_id) REFERENCES airport_terminals (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_1C916B648DB82A2 ON taxi_requests (airport_id_id)');
        $this->addSql('CREATE INDEX IDX_1C916B64CF2FC8D0 ON taxi_requests (terminal_id_id)');
    }
}
