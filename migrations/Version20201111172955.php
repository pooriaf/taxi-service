<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201111172955 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE airport_terminals (id INT AUTO_INCREMENT NOT NULL, airport_id_id INT NOT NULL, code VARCHAR(255) NOT NULL, INDEX IDX_86660BD38DB82A2 (airport_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE airports (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taxi_requests (id INT AUTO_INCREMENT NOT NULL, airport_id_id INT NOT NULL, terminal_id_id INT DEFAULT NULL, full_name VARCHAR(255) NOT NULL, mobile VARCHAR(15) NOT NULL, arrival_date DATE NOT NULL, airflight_number VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_1C916B648DB82A2 (airport_id_id), INDEX IDX_1C916B64CF2FC8D0 (terminal_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE airport_terminals ADD CONSTRAINT FK_86660BD38DB82A2 FOREIGN KEY (airport_id_id) REFERENCES airports (id)');
        $this->addSql('ALTER TABLE taxi_requests ADD CONSTRAINT FK_1C916B648DB82A2 FOREIGN KEY (airport_id_id) REFERENCES airports (id)');
        $this->addSql('ALTER TABLE taxi_requests ADD CONSTRAINT FK_1C916B64CF2FC8D0 FOREIGN KEY (terminal_id_id) REFERENCES airport_terminals (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE taxi_requests DROP FOREIGN KEY FK_1C916B64CF2FC8D0');
        $this->addSql('ALTER TABLE airport_terminals DROP FOREIGN KEY FK_86660BD38DB82A2');
        $this->addSql('ALTER TABLE taxi_requests DROP FOREIGN KEY FK_1C916B648DB82A2');
        $this->addSql('DROP TABLE airport_terminals');
        $this->addSql('DROP TABLE airports');
        $this->addSql('DROP TABLE taxi_requests');
    }
}
