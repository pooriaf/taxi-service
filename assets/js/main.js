import Vue from 'vue';
import Vuelidate from 'vuelidate'
import TaxiRequest from './components/TaxiRequest.vue';
import '../styles/app.css';

Vue.use(Vuelidate);
new Vue({
    el: '#app',
    render: h => h(TaxiRequest)
});

