<?php

namespace App\TaxiService;

use App\Entity\TaxiRequest;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class EmailNotifier
{
    const TEMPLATE_PATH = 'emails/request_sent.html.twig';

    private $mailer;
    private $from;
    private $to;

    public function __construct(MailerInterface $mailer, string $from, string $to)
    {
        $this->mailer = $mailer;
        $this->from = $from;
        $this->to = $to;
    }

    public function send(TaxiRequest $taxiRequest)
    {
        $email = (new TemplatedEmail())
            ->from($this->from)
            ->to($this->to)
            ->priority(Email::PRIORITY_HIGH)
            ->subject("New taxi request for {$taxiRequest->getArrivalDate()->format('Y-m-d')}, NUM: {$taxiRequest->getAirflightNumber()}")
            ->htmlTemplate(self::TEMPLATE_PATH)
            ->context(
                [
                    'id' => $taxiRequest->getId(),
                    'full_name' => $taxiRequest->getFullName(),
                    'mobile' => $taxiRequest->getMobile(),
                    'arrival_date' => $taxiRequest->getArrivalDate()->format('Y-m-d'),
                    'airflight_number' => $taxiRequest->getAirflightNumber(),
                    'airport_name' => $taxiRequest->getAirport()->getName(),
                    'terminal' => $taxiRequest->getTerminal() ? $taxiRequest->getTerminal()->getCode() : '-'
                ]
            );

        $this->mailer->send($email);
    }
}