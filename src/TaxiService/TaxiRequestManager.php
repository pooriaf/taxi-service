<?php

namespace App\TaxiService;

use App\Entity\Airport;
use App\Entity\AirportTerminal;
use App\Entity\TaxiRequest;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class TaxiRequestManager
{
    private $entityManager;
    private $airportRepo;
    private $terminalRepo;

    private $emailNotifier;

    public function __construct(EntityManagerInterface $entityManager, EmailNotifier $emailNotifier)
    {
        $this->entityManager = $entityManager;
        $this->airportRepo = $entityManager->getRepository(Airport::class);
        $this->terminalRepo = $entityManager->getRepository(AirportTerminal::class);
        $this->emailNotifier = $emailNotifier;
    }

    public function make(array $taxiRequest): TaxiRequest
    {
        /** @var Airport $airport */
        $airport = $this->airportRepo->find($taxiRequest['airport_id']);
        /** @var AirportTerminal $airportTerminal */
        $airportTerminal = $taxiRequest['terminal_id'] ? $this->terminalRepo->find($taxiRequest['terminal_id']) : null;
        if (!$airport || ($airportTerminal && $airportTerminal->getAirport()->getId() != $airport->getId())) {
            throw new UnprocessableEntityHttpException('Invalid Airport/Terminal');
        }
        $now = new \DateTime('today');
        $arrivalDate = new \DateTime($taxiRequest['arrival_date']);
        if ($arrivalDate < $now) {
            throw new UnprocessableEntityHttpException('Arrival date is for a past date');
        }

        $taxiRequestEntity = new TaxiRequest();
        $taxiRequestEntity
            ->setFullName($taxiRequest['full_name'])
            ->setMobile($taxiRequest['mobile'])
            ->setArrivalDate($arrivalDate)
            ->setAirflightNumber($taxiRequest['airflight_number'])
            ->setAirport($airport)
            ->setTerminal($airportTerminal)
            ->setCreatedAt($now);
        $this->entityManager->persist($taxiRequestEntity);
        $this->entityManager->flush();

        $this->emailNotifier->send($taxiRequestEntity);
        return $taxiRequestEntity;
    }
}