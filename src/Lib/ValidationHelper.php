<?php

namespace App\Lib;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationHelper
{
    public static function formatCollectionViolations(ConstraintViolationListInterface $violationList): array
    {
        $errors = [];
        foreach ($violationList as $violation) {
            $errors[] = ['key' => $violation->getPropertyPath(), 'message' => $violation->getMessage()];
        }

        return [
            'errors' => $errors
        ];
    }
}