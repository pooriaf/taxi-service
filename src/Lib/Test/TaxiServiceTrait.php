<?php

namespace App\Lib\Test;

use App\Entity\Airport;
use App\Entity\AirportTerminal;
use App\Entity\TaxiRequest;

trait TaxiServiceTrait
{
    public function getDummyRequestArray(int $airportId = 1): array
    {
        return [
            'full_name' => 'Pooria Farhad',
            'mobile' => '07419250251',
            'airport_id' => $airportId,
            'terminal_id' => null,
            'arrival_date' => (new \DateTime())->format('Y-m-d'),
            'airflight_number' => '2A333'
        ];
    }

    public function getDummyRequestObject(Airport $airport, ?AirportTerminal $airportTerminal = null): TaxiRequest
    {
        $dummyArray = $this->getDummyRequestArray($airport->getId());

        $taxiRequest = new TaxiRequest();
        $taxiRequest->setFullName($dummyArray['full_name']);
        $taxiRequest->setMobile($dummyArray['mobile']);
        $taxiRequest->setAirport($airport);
        $taxiRequest->setTerminal($airportTerminal);
        $taxiRequest->setArrivalDate(new \DateTime($dummyArray['arrival_date']));
        $taxiRequest->setAirflightNumber($dummyArray['airflight_number']);

        return $taxiRequest;
    }
}