<?php

namespace App\DataFixtures;

use App\Entity\Airport;
use App\Entity\AirportTerminal;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AirportFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $heathrow = new Airport();
        $heathrow->setName('Heathrow');
        $manager->persist($heathrow);
        for ($i = 1; $i <= 4; $i++) {
            $airportTerminal = new AirportTerminal();
            $airportTerminal->setAirport($heathrow);
            $airportTerminal->setCode($i);
            $manager->persist($airportTerminal);
        }

        $gatwick = new Airport();
        $gatwick->setName('Gatwick');
        $manager->persist($gatwick);


        $manager->flush();
    }
}
