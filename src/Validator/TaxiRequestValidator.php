<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Type;

class TaxiRequestValidator
{
    public static function getCollection(): Collection
    {
        return new Collection(
            [
                'full_name' => new NotBlank(),
                'mobile' => new Regex(['pattern' => '/^07\d{9}$/']),
                'arrival_date' => new Date(),
                'airflight_number' => new Regex(['pattern' => '/^([A-Z]{2}|[A-Z]\d|\d[A-Z])\s?\d{3,4}$/']),
                'airport_id' => new Type(['type' => 'numeric']),
                'terminal_id' => new Type(['type' => ['null', 'numeric']])
            ]
        );
    }
}