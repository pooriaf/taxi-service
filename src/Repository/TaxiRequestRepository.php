<?php

namespace App\Repository;

use App\Entity\TaxiRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TaxiRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaxiRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaxiRequest[]    findAll()
 * @method TaxiRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaxiRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaxiRequest::class);
    }

    // /**
    //  * @return TaxiRequest[] Returns an array of TaxiRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaxiRequest
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
