<?php

namespace App\Repository;

use App\Entity\AirportTerminal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AirportTerminal|null find($id, $lockMode = null, $lockVersion = null)
 * @method AirportTerminal|null findOneBy(array $criteria, array $orderBy = null)
 * @method AirportTerminal[]    findAll()
 * @method AirportTerminal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AirportTerminalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AirportTerminal::class);
    }

    public function findAirportTerminals(int $airportId): array
    {
        return $this->createQueryBuilder('at')
            ->andWhere('at.airport = :val')
            ->setParameter('val', $airportId)
            ->orderBy('at.id', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }
}
