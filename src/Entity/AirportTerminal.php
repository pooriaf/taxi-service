<?php

namespace App\Entity;

use App\Repository\AirportTerminalRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AirportTerminalRepository::class)
 * @ORM\Table(name="airport_terminals")
 */
class AirportTerminal
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity=Airport::class, inversedBy="airportTerminals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $airport;

    /**
     * @ORM\OneToMany(targetEntity=TaxiRequest::class, mappedBy="terminal_id")
     */
    private $taxiRequests;

    public function __construct()
    {
        $this->taxiRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getAirport(): ?Airport
    {
        return $this->airport;
    }

    public function setAirport(?Airport $airport): self
    {
        $this->airport = $airport;

        return $this;
    }

    /**
     * @return Collection|TaxiRequest[]
     */
    public function getTaxiRequests(): Collection
    {
        return $this->taxiRequests;
    }

    public function addTaxiRequest(TaxiRequest $taxiRequest): self
    {
        if (!$this->taxiRequests->contains($taxiRequest)) {
            $this->taxiRequests[] = $taxiRequest;
            $taxiRequest->setTerminal($this);
        }

        return $this;
    }

    public function removeTaxiRequest(TaxiRequest $taxiRequest): self
    {
        if ($this->taxiRequests->removeElement($taxiRequest)) {
            // set the owning side to null (unless already changed)
            if ($taxiRequest->getTerminal() === $this) {
                $taxiRequest->setTerminal(null);
            }
        }

        return $this;
    }
}
