<?php

namespace App\Entity;

use App\Repository\AirportRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AirportRepository::class)
 * @ORM\Table(name="airports")
 */
class Airport
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=AirportTerminal::class, mappedBy="airport_id", orphanRemoval=true)
     */
    private $airportTerminals;

    /**
     * @ORM\OneToMany(targetEntity=TaxiRequest::class, mappedBy="airport_id", orphanRemoval=true)
     */
    private $taxiRequests;

    public function __construct()
    {
        $this->airportTerminals = new ArrayCollection();
        $this->taxiRequests = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|AirportTerminal[]
     */
    public function getAirportTerminals(): Collection
    {
        return $this->airportTerminals;
    }

    public function addAirportTerminal(AirportTerminal $airportTerminal): self
    {
        if (!$this->airportTerminals->contains($airportTerminal)) {
            $this->airportTerminals[] = $airportTerminal;
            $airportTerminal->setAirport($this);
        }

        return $this;
    }

    public function removeAirportTerminal(AirportTerminal $airportTerminal): self
    {
        if ($this->airportTerminals->removeElement($airportTerminal)) {
            // set the owning side to null (unless already changed)
            if ($airportTerminal->getAirport() === $this) {
                $airportTerminal->setAirport(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TaxiRequest[]
     */
    public function getTaxiRequests(): Collection
    {
        return $this->taxiRequests;
    }

    public function addTaxiRequest(TaxiRequest $taxiRequest): self
    {
        if (!$this->taxiRequests->contains($taxiRequest)) {
            $this->taxiRequests[] = $taxiRequest;
            $taxiRequest->setAirport($this);
        }

        return $this;
    }

    public function removeTaxiRequest(TaxiRequest $taxiRequest): self
    {
        if ($this->taxiRequests->removeElement($taxiRequest)) {
            // set the owning side to null (unless already changed)
            if ($taxiRequest->getAirport() === $this) {
                $taxiRequest->setAirport(null);
            }
        }

        return $this;
    }
}
