<?php

namespace App\Entity;

use App\Repository\TaxiRequestRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TaxiRequestRepository::class)
 * @ORM\Table(name="taxi_requests")
 */
class TaxiRequest
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $full_name;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $mobile;

    /**
     * @ORM\Column(type="date")
     */
    private $arrival_date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $airflight_number;

    /**
     * @ORM\ManyToOne(targetEntity=Airport::class, inversedBy="taxiRequests")
     * @ORM\JoinColumn(nullable=false)
     */
    private $airport;

    /**
     * @ORM\ManyToOne(targetEntity=AirportTerminal::class, inversedBy="taxiRequests")
     */
    private $terminal;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->full_name;
    }

    public function setFullName(string $full_name): self
    {
        $this->full_name = $full_name;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getArrivalDate(): ?\DateTimeInterface
    {
        return $this->arrival_date;
    }

    public function setArrivalDate(\DateTimeInterface $arrival_date): self
    {
        $this->arrival_date = $arrival_date;

        return $this;
    }

    public function getAirflightNumber(): ?string
    {
        return $this->airflight_number;
    }

    public function setAirflightNumber(string $airflight_number): self
    {
        $this->airflight_number = $airflight_number;

        return $this;
    }

    public function getAirport(): ?Airport
    {
        return $this->airport;
    }

    public function setAirport(?Airport $airport): self
    {
        $this->airport = $airport;

        return $this;
    }

    public function getTerminal(): ?AirportTerminal
    {
        return $this->terminal;
    }

    public function setTerminal(?AirportTerminal $terminal): self
    {
        $this->terminal = $terminal;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }
}
