<?php

namespace App\Controller;

use App\Lib\ValidationHelper;
use App\TaxiService\TaxiRequestManager;
use App\Validator\TaxiRequestValidator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Routing\Annotation\Route;

class TaxiRequestController extends AbstractController
{
    /**
     * @Route("api/taxi-requests", methods={"POST"})
     */
    public function storeAction(Request $request, ValidatorInterface $validator, TaxiRequestManager $taxiRequestManager): Response
    {
        $taxiRequestInput = json_decode($request->getContent(), true);
        $violations = $validator->validate($taxiRequestInput, TaxiRequestValidator::getCollection());
        if ($violations->count() > 0) {
            return $this->json(ValidationHelper::formatCollectionViolations($violations), Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $taxiRequest = $taxiRequestManager->make($taxiRequestInput);

        return $this->json(['id' => $taxiRequest->getId()]);
    }
}
