<?php

namespace App\Controller;

use App\Repository\AirportRepository;
use App\Repository\AirportTerminalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AirportsController extends AbstractController
{
    /**
     * @Route("api/airports", methods={"GET"})
     */
    public function indexAction(AirportRepository $airportRepository): Response
    {
        $airports = $airportRepository->findAllAirports();

        return $this->json($airports);
    }

    /**
     * @Route("/api/airports/{airportId}/terminals", methods={"GET"}, requirements={"airportId"="\d+"})
     */
    public function getAirportTerminalsAction(int $airportId, AirportTerminalRepository $airportTerminalRepository): Response
    {
        $airports = $airportTerminalRepository->findAirportTerminals($airportId);

        return $this->json($airports);
    }
}
